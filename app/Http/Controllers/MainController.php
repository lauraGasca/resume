<?php namespace Resume\Http\Controllers;

use Illuminate\Http\Request;
use Resume\Http\Requests;
use Resume\Http\Requests\ContactForm;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Resume\Http\Requests\ContactForm  $request
     * @return \Illuminate\Http\Response
     */
    public function send(ContactForm $request)
    {
        //Send Mail
        $info = $request->all();

        \Mail::send('emails.contact', ['info'=>$info], function($message) use ($info)
        {
            //remitente
            $message->to(env('CONTACT_MAIL'), env('CONTACT_NAME'));

            //asunto
            $message->subject('Contacto desde Sitio Web');

            //receptor
            $message->from('lauhachi@gmail.com', 'Laura Gasca');

        });
        return redirect()->back()->with(array('confirm' => 'Se ha registrado correctamente.'));
    }
}
