<!DOCTYPE html>
<html lang="es">
    <head>
        @include('partials.styles')
    </head>
    <body>
        @include('partials.header')
        @include('partials.main')
        @include('partials.skill')
        @include('partials.education')
        @include('partials.experience')
        @include('partials.portafolio')
        @include('partials.contact')
        @include('partials.footer')
        @include('partials.scripts')
    </body>
</html>