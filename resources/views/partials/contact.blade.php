<section id="contact-section" class="contact-section">
    <div class="container">
        <div class="row">
            <div class="section-title">Contacto</div>
            @if(Session::get('confirm')||count($errors)>0)
                <script>
                    location.href = "#contact-section"
                </script>
            @endif
            @if(Session::get('confirm'))
                <div class="alert alert-success bg-success">
                    <strong>¡Gracias por tu mensaje!</strong> Nos pondremos en contacto contigo a la brevedad.
                </div>
            @endif
            @if(count($errors)>0)
                <div class="alert alert-error bg-danger" id="peligro">
                    <strong>¡Error!</strong> Por favor revisa los datos del formulario.
                </div>
            @endif
            <!-- contact starts -->
            <div class="col l5 m5 s12 contact-data pdl-0">
                <div class="col s12 w-block z-depth-1 shadow-change pd-0">
                    <div class="col s12 pdt-30 pdr-30 pdb-10 pdl-30">
                        <div class="arrow-box">Información de Contacto</div>
                        <div class="c-info"><span class="fa fa-phone"></span><span>(412) 108-1013</span></div>
                        <div class="c-info"><span class="fa fa-road"></span><span>Juventino Rosas, Guanajuato</span></div>
                        <div class="c-info"><span class="fa fa-envelope"></span><span>lau_lost@hotmail.com</span></div><br/><br/>
                        <div class="col s12 g-map-wrapper pd-0"><div id="g-map" data-latitude="20.644081" data-longitude="-100.993211"></div></div>
                        <div class="col s12 contact-map-btn z-depth-1 shadow-change waves-effect waves-light pd-0"><span class="fa fa-map-marker"></span>Ver Mapa</div>
                        <br/><br/><br/><br/><br/><br/>
                    </div>
                </div>
            </div>
            <div class="col l7 m7 s12 contact-form pdr-0">
                <div class="col s12 w-block z-depth-1 shadow-change pdt-10 pdr-30 pdb-30 pdl-30">
                    {!! Form::open(['route'=>'send', 'method' => 'post', 'class'=>"c-form"]) !!}
                        <fieldset>
                            <div class="input-field">
                                {!! Form::text('name', null, ['class'=>"validate", 'id'=>"name"]) !!}
                                <label for="name">Nombre</label>
                            </div>
                            <span class="help-block">{{ $errors->first('name') }}</span>
                            <div class="input-field">
                                {!! Form::email('email', null, ['class'=>"validate", 'id'=>"email"]) !!}
                                <label for="email">Correo Electronico</label>
                            </div>
                            <span class="help-block">{{ $errors->first('email') }}</span>
                            <div class="input-field">
                                {!! Form::textarea('message', null, ['rows'=>'2', 'id'=>"message", 'class'=>"materialize-textarea validate"]) !!}
                                <label for="message">Mensaje</label>
                            </div>
                            <span class="help-block">{{$errors->first('message')}}</span>
                            <div class="input-field">
                                {!! Recaptcha::render() !!}
                            </div>
                            <span class="help-block">{{$errors->first('g-recaptcha-response')}}</span>
                            <div>
                                <button class="btn waves-effect waves-light" type="submit" name="button" style="margin-top: 20px;">Enviar Mensaje</button>
                                <span id="c-form-status" class="hidden"></span>
                            </div>
                        </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- contact ends -->
        </div>
    </div>
</section>