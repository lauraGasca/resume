<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <a class="btn btn-floating waves-effect waves-light back-to-top animatescroll-link" onclick="$('html').animatescroll();" href="#0">
                    <span class="fa fa-angle-up"></span>
                </a>
                <div class="social-links">
                    @include('partials.social')
                </div>
            </div>
        </div>
    </div>
</footer>