<section id="experience-section" class="experience-section">
    <div class="container">
        <div class="row">
            <div class="section-title">Experiencia</div>
            <div class="col s12 section-content pd-0">
                <!-- experience starts -->
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge"><a><i class="fa fa-circle"></i></a></div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">Desarrollador Web</div>
                            <div class="timeline-tag">Computación Integral del Bajío</div>
                            <div class="timeline-desc"><p>Desarrollo y mantenimiento  de sitios y aplicaciones Web, tanto para la empresa como para sus clientes.</p></div>
                            <div class="timeline-time">Noviembre 2015 -  Actualmente</div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge"><a><i class="fa fa-circle invert"></i></a></div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">Desarrollador Web</div>
                            <div class="timeline-tag">IncubaMás</div>
                            <div class="timeline-desc"><p>Desarrollo y mantenimiento  de sitios y aplicaciones Web, tanto para la empresa como para sus clientes.</p></div>
                            <div class="timeline-time">Mayo 2014 – Febrero 2015</div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge"><a><i class="fa fa-circle"></i></a></div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">Analista de Mercado</div>
                            <div class="timeline-tag">Schneider Electric</div>
                            <div class="timeline-desc"><p>Analista de mercado, Programadora de aplicación Web en Servidor interno durante el periodo de residencias profesionales.</p></div>
                            <div class="timeline-time">Julio 2013 – Abril 2014</div>
                        </div>
                    </li>

                    <li class="clearfix no-float"></li>
                </ul>
                <!-- experience ends -->
            </div>
        </div>
    </div>
</section>