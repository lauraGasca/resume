<div class="site-header z-depth-1 top-section">
    <div class="container">
        <div class="row">
            <div class="col l6 m6 s12 pd-0"><div class="site-header-title">Laura Critina Gasca Villafaña<span>Desarrollador Web BackEnd</span></div></div>
            <div class="col l6 m6 s12 pd-0">
                <div class="site-header-contact">
                    @include('partials.social')
                </div>
            </div>
        </div>
    </div>
</div>

<section id="overview-section" class="overview-section">
    <div class="section-content">
        <div class="container">
            <div class="row">
                <div class="col s12 about-section w-block z-depth-1 shadow-change pd-0 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
                    <div class="col l4 m6 s12 about-img pd-0 image-bg" data-image-bg="{{asset('img/laura-gasca-resume.jpg')}}">
                        <div class="about-more">
                            <div class="about-more-content">
                                <a class="btn btn-floating btn-large tooltipped" href="{{asset('img/LauraGascaResume.pdf')}}" target="_blank" data-position="top" data-delay="50" data-tooltip="Descargar CV"><span class="fa fa-download"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col l8 m6 s12 about-data pd-0">
                        <div class="about-desc pd-30">
                            <div class="overview-name"><span>Laura Critina Gasca Villafaña</span></div>
                            <div class="overview-title">Desarrollador Web BackEnd</div>
                            <div class="overview-data">
                                <div><span>Edad</span>25 años</div>
                                <div><span>Dirección</span>Juventino Rosas, Guanajuato</div>
                                <div><span>Correo</span>lau_lost@hotmail.com</div>
                                <div><span>Teléfono</span>(412) 108-1013</div>
                            </div>
                        </div>
                        <div class="about-social col s12 pd-0" style="text-align: center;">
                            @include('partials.social')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>