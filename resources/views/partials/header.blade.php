<div class="preloader">
    <div class="preloader-inner">
        <div class="preloader-wrapper active">
            <div class="spinner-layer">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="gap-patch">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header header-hidden z-depth-1 shadow-change">
    <div class="site-logo"><a href="#">Laura Cristina Gasca Villafaña</a></div>
    <div class="menu-bar btn-floating waves-effect waves-light"><span class="fa fa-bars"></span></div>
    <nav class="main-nav">
        <ul>
            <li><a href="#0" class="animatescroll-link waves-effect" onclick="$('html').animatescroll();">Inicio</a></li>
            <li><a href="#0" class="skill-section-nav animatescroll-link waves-effect" onclick="$('#skill-section').animatescroll();">Conocimientos</a></li>
            <li><a href="#0" class="education-section-nav animatescroll-link waves-effect" onclick="$('#education-section').animatescroll();">Educación</a></li>
            <li><a href="#0" class="experience-section-nav animatescroll-link waves-effect" onclick="$('#experience-section').animatescroll();">Experiencia</a></li>
            <li><a href="#0" class="portfolio-section-nav animatescroll-link waves-effect" onclick="$('#portfolio-section').animatescroll();">Portafolio</a></li>
            <li><a href="#0" class="contact-section-nav animatescroll-link waves-effect" onclick="$('#contact-section').animatescroll();">Contacto</a></li>
        </ul>
    </nav>
</header>