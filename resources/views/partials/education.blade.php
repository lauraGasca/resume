<section id="education-section" class="education-section">
    <div class="container">
        <div class="row">
            <div class="section-title">Educación</div>
            <div class="col s12 section-content pd-0">
                <!-- education starts -->
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge"><a><i class="fa fa-circle"></i></a></div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">Ingeniería en Sistemas Computacionales</div>
                            <div class="timeline-tag">Instituto Tecnológico de Celaya</div>
                            <div class="timeline-time">2009 - 2013</div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge"><a><i class="fa fa-circle invert"></i></a></div>
                        <div class="timeline-panel w-block z-depth-1 shadow-change pd-30">
                            <div class="timeline-title">Tecnólogo en Programación de Equipo de Computo</div>
                            <div class="timeline-tag">Centro de Estudios Tecnológicos No. 155 Mexicano- Japonés</div>
                            <div class="timeline-time">2005 - 2009</div>
                        </div>
                    </li>
                    <li class="clearfix no-float"></li>
                </ul>
                <!-- education ends -->
            </div>
        </div>
    </div>
</section>