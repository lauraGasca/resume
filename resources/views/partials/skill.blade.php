<section id="skill-section" class="skill-section">
    <div class="container">
        <div class="row">
            <div class="section-title">Habilidades</div>
            <div class="col s12 section-content skill-wrapper w-block z-depth-1 shadow-change pd-50">
                <!-- skill starts -->
                <div class="col m12" style="margin-bottom: 27px;">
                    <div class="col l6 m6 s12 skill-desc pdl-0" style="padding: 0 25px;">
                        <p>
                            Soy una apasionada de la programación, con 3 años de experiencia en desarrollo de aplicaciones web.
                            Mi enfoque es hacia el backend para lo cual estoy en constante capacitación. Mi meta como profesional
                            es coordinar equipos de trabajo de desarrollo. Soy muy constante, disciplinada, ordenada y me gusta que
                            mi equipo sienta la misma pasión por su trabajo.
                        </p><br/>
                    </div>
                    <div class="col l6 m6 s12 skill-data pdr-0" style="padding: 0 25px;">
                        <div class="progress-bar-wrapper">
                            <p class="progress-text">HTML, CSS <small>6 años</small><span>85%</span></p>
                            <div class="progress-bar"><span data-percent="85"></span></div>
                        </div>
                        <div class="progress-bar-wrapper">
                            <p class="progress-text">PHP <small>4 años</small><span>65%</span></p>
                            <div class="progress-bar"><span data-percent="65"></span></div>
                        </div>
                        <div class="progress-bar-wrapper">
                            <p class="progress-text">Laravel 4 y 5 <small>2 años</small><span>50%</span></p>
                            <div class="progress-bar"><span data-percent="50"></span></div>
                        </div>
                        <div class="progress-bar-wrapper">
                            <p class="progress-text">Git, Github, Bitbucket <small>2 años</small><span>60%</span></p>
                            <div class="progress-bar"><span data-percent="60"></span></div>
                        </div>
                    </div>
                </div>
                <div class="col l6 m6 s12 skill-data pdr-0" style="padding: 0 25px;">
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Wordpress <small>1 año</small><span>40%</span></p>
                        <div class="progress-bar"><span data-percent="40"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Twitter Bootstrap <small>1 año</small><span>65%</span></p>
                        <div class="progress-bar"><span data-percent="65"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">MS SQL server <small>2 años</small><span>70%</span></p>
                        <div class="progress-bar"><span data-percent="70"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Java <small>2 años</small><span>40%</span></p>
                        <div class="progress-bar"><span data-percent="40"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">JS, jQuery <small>5 años</small><span>45%</span></p>
                        <div class="progress-bar"><span data-percent="45"></span></div>
                    </div>
                </div>
                <div class="col l6 m6 s12 skill-data pdr-0" style="padding: 0 25px;">
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Ajax, JSON, API Rest<small>2 años</small><span>60%</span></p>
                        <div class="progress-bar"><span data-percent="60"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Consola Linux <small>2 años</small><span>65%</span></p>
                        <div class="progress-bar"><span data-percent="65"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Angularjs <small>4 meses</small><span>35%</span></p>
                        <div class="progress-bar"><span data-percent="35"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">MySQL <small>5 años</small><span>80%</span></p>
                        <div class="progress-bar"><span data-percent="80"></span></div>
                    </div>
                    <div class="progress-bar-wrapper">
                        <p class="progress-text">Testing en Laravel <small>3 Meses</small><span>30%</span></p>
                        <div class="progress-bar"><span data-percent="30"></span></div>
                    </div>
                </div>
                <!-- skill ends -->
            </div>
        </div>
    </div>
</section>