<meta name="description" content="desarrollador web backend">
<meta charset="UTF-8">
<meta name="author" content="Laura Gasca">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Laura Cristina Gasca Villafaña | CV</title>
<link rel="shortcut icon" href="{{asset('img/favicon.png')}}"/>
{!! Html::style('https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300italic,300,900italic,900,700italic,700,500italic,500,400italic') !!}
{!! Html::style('font/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('css/animate.css') !!}
{!! Html::style('css/materialize.min.css') !!}
{!! Html::style('css/magnific-popup.css') !!}
{!! Html::style('css/owl.carousel.css') !!}
{!! Html::style('css/owl.theme.css') !!}
{!! Html::style('css/owl.transitions.css') !!}
{!! Html::style('css/style.css') !!}
{!! Html::style('css/color-1.css') !!}
{!! Html::style('css/responsive.css') !!}
{!! Html::script('js/modernizr.js') !!}