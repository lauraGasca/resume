<section id="portfolio-section" class="portfolio-section">
    <div class="container">
        <div class="row">
            <div class="section-title">Portfolio</div>
            <div class="col s12 section-content pd-0">
                <!-- portfolio starts -->
                <ul class="filter">
                    <li><a class="active" href="#" data-filter="*">Todos</a></li>
                    <li><a href="#" data-filter=".design">Sitio Web</a></li>
                    <li><a href="#" data-filter=".development">Aplicación Web</a></li>
                    <li><a href="#" data-filter=".maintenance">Mantenimiento</a></li>
                    <li><a href="#" data-filter=".redesign">Rediseño</a></li>
                </ul>
                <ul class="portfolio-items">
                    <li class="design portfolio-content">
                        <figure>
                            <img src="{{asset('img/portfolio/facturizalo.png')}}" alt="Facturizalo.com">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a href="http://facturizalo.com/" target="_blank"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">Facturizalo.com</div>
                                    <div class="portfolio-intro-category"><p>Sitio Web</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                    <li class="design portfolio-content three-col-portfolio-gutter">
                        <figure>
                            <img src="{{asset('img/portfolio/aitecsa.png')}}" alt="AITECSA">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="http://aitecsa.com.mx/" target="_blank"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">AITECSA</div>
                                    <div class="portfolio-intro-category"><p>Sitio Web</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                    <li class="design development portfolio-content three-col-portfolio-gutter">
                        <figure>
                            <img src="{{asset('img/portfolio/pro-pan.png')}}" alt="Panel Triunfo">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="http://paneltriunfo.com/" target="_blank"><span class="fa fa-link"></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">Pan el Triunfo</div>
                                    <div class="portfolio-intro-category"><p>Sitio Web</p><p>Aplicación Web</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                    <li class="design development portfolio-content three-col-portfolio-gutter">
                        <figure>
                            <img src="{{asset('img/portfolio/pro-con.png')}}" alt="Consorcio del Conocimiento">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="http://conconocimiento.com/" target="_blank"><span class="fa fa-link"></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">Consorcio del Conocimiento</div>
                                    <div class="portfolio-intro-category"><p>Sitio Web</p><p>Aplicación Web</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                    <li class="maintenance redesign portfolio-content three-col-portfolio-gutter">
                        <figure>
                            <img src="{{asset('img/portfolio/laquebuena.png')}}" alt="La Que Buena Atlanta">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a href="http://www.laquebuenaatlanta.com/" target="_blank"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">La Que Buena Atlanta</div>
                                    <div class="portfolio-intro-category"><p>Rediseño</p><p>Mantenimiento</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                    <li class="design development portfolio-content three-col-portfolio-gutter">
                        <figure>
                            <img src="{{asset('img/portfolio/pro-incuba.png')}}" alt="image">
                            <figcaption>
                                <div class="portfolio-intro">
                                    <div class="portfolio-intro-link">
                                        <a  href="http://incubamas.com/" target="_blank"><span><i class="fa fa-link"></i></span></a>
                                    </div>
                                    <div class="portfolio-intro-title">IncubaMás</div>
                                    <div class="portfolio-intro-category"><p>Sitio Web</p><p>Aplicación Web</p></div>
                                </div>
                            </figcaption>
                        </figure>
                    </li>
                </ul>
                <!-- portfolio ends -->
            </div>
        </div>
    </div>
</section>